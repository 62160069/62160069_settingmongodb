const mongoose = require('mongoose')
const Building = require('./models/building')
const Room = require('./models/room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const newInformaticsBuling = await Building.findById('621a127360211bba192a5cb4')
  const rooms = await Room.findById('621a127460211bba192a5cba')
  const informatics = await Building.findById(rooms.building)
  console.log(rooms)
  console.log('---------------------------------')
  console.log(newInformaticsBuling)
  console.log('---------------------------------')
  console.log(informatics)
  rooms.building = newInformaticsBuling
  newInformaticsBuling.room.push(rooms)
  informatics.room.pull(rooms)
  newInformaticsBuling.save()
  rooms.save()
  informatics.save()
}
main().then(() => {
  console.log('finish')
})
